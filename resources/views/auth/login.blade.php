@extends('layouts.app')

@section('content')
<b-container>
    <b-row align-h="center">

        <b-col cols="8">

            <b-card title="Inicio de sesión" class="my-1">
                
                @if ($errors->any())
                <b-alert show variant="danger">
                    <ul class="mb-0">
                      @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                      @endforeach
                    </ul>
                </b-alert>
                @else
                <b-alert variant="info" show>Por favor Ingresa tus Datos :)</b-alert>
                @endif
                <b-form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <b-form-group
                                  label="Correo electrónico:"
                                  label-for="email"
                                  description="Por favor Ingresa tu correo electrónico.">
                        <b-form-input type="email"
                                      id="email" name="email"
                                      required autofocus
                                      placeholder="Ingresa aquí tu correo">
                        </b-form-input>
                    </b-form-group>

                    <b-form-group label="Contraseña" label-for="password">
                        <b-form-input type="password"
                                      id="password" name="password"
                                      required>
                        </b-form-input>
                    </b-form-group>


                    <b-form-group>
                        <b-form-checkbox value="remember" {{ old('remember') ? 'checked="true"' : '' }}>
                            Recordarme
                        </b-form-checkbox>
                    </b-form-group>


                        <b-button type="submit" variant="primary">
                            Ingresar</b-button>


                        <b-button  href="{{ route('password.request') }}" variant="link">
                            ¿Olvidaste tu contraseña?</b-button>

                        <a class="btn btn-link">

                        </a>


                </b-form>

            </b-card>


            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">

                </div>
            </div>
        </b-col>

    </b-row>
</b-container>
@endsection

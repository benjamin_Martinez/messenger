@extends('layouts.app')

@section('content')
    <b-container>
        <b-row align-h="center">

            <b-col cols="8">
                <b-card title="Registro" class="my-1">



                    @if($errors->any())
                    <b-alert show variant="danger">
                        @foreach($errors->all() as $error)
                        <ul class="mb-0">

                                <li>{{ $error }}</li>

                        </ul>
                        @endforeach
                    </b-alert>

                    @else

                        <b-alert variant="info" show>
                            Por favor Ingresa tus Datos :
                        </b-alert>

                    @endif
                    <b-form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <b-form-group
                                label="Nombre:"
                                label-for="name"
                                description="Por favor Ingresa tu nombre.">
                            <b-form-input type="text"
                                          id="name" name="name"
                                          value="{{ old('name') }}" required autofocus>
                            </b-form-input>
                        </b-form-group>


                        <b-form-group
                                label="Correo electrónico:"
                                label-for="email"
                                description="Por favor Ingresa tu correo electrónico.">
                            <b-form-input type="email"
                                          id="email" name="email"
                                          value="{{ old('email') }}" required autofocus
                                          placeholder="Ingresa aquí tu correo">
                            </b-form-input>
                        </b-form-group>

                        <b-form-group label="Contraseña" label-for="password">
                            <b-form-input type="password"
                                          id="password" name="password"
                                          required>
                            </b-form-input>
                        </b-form-group>

                        <b-form-group label="Confirmar contraseña" label-for="passwordconfirmation">
                            <b-form-input type="password"
                                          id="password_confirmation" name="passwordconfirmation"
                                          required>
                            </b-form-input>
                        </b-form-group>

                        <b-button type="submit" variant="primary">
                            Confirmar Registro</b-button>


                        <b-button  href="{{ route('login') }}" variant="link">
                            ¿Ya te has registrado?</b-button>

                        <a class="btn btn-link">

                        </a>


                    </b-form>

                </b-card>

            </b-col>
        </b-row>
    </b-container>

@endsection
